<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/status/list', [\App\Http\Controllers\Crud\StatusController::class, 'statusList'])->name('status.list');
Route::get('/users/list', [\App\Http\Controllers\Crud\UserController::class, 'getListUsers'])->name('users.list');
Route::get('/tasks/filter/list', [\App\Http\Controllers\FrontController::class, 'getTask'])->name('task.list');

Route::apiResource('tasks', \App\Http\Controllers\Crud\TaskController::class);
