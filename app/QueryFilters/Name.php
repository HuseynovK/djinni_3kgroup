<?php


namespace App\QueryFilters;


class Name extends Filter
{

    protected function applyFilters($builder)
    {
        $title = request($this->filterName());
        if ($title) {
            return $builder->where('name', 'like', '%' . $title. '%');
        }

        return $builder;
    }
}
