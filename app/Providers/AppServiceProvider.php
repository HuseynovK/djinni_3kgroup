<?php

namespace App\Providers;

use App\Models\Task;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('statuscheck', function ($attribute, $value, $parameters, $validator) {
            return array_key_exists($value, Task::TASK_STATUS_ARRAY);
        }, 'Status not Defined');
    }
}
