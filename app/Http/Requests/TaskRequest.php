<?php

namespace App\Http\Requests;

use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest
{
    protected $required;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->required = request()->method() == 'post' ? 'post' : 'put';
        $this->merge([
            'deadline' => Carbon::parse($this->deadline)->format('yy-m-d')
        ]);
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'deadline' => 'required|date',
            'description' => 'required',
            'status_id' => 'nullable|integer|statuscheck',
        ];
    }
}
