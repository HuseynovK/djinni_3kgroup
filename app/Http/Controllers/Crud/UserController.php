<?php

namespace App\Http\Controllers\Crud;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getListUsers()
    {
        $users = User::orderByDesc('created_at')->get();
        return UserResource::collection($users);
    }
}
