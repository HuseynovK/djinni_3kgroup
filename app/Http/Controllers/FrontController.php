<?php

namespace App\Http\Controllers;

use App\Http\Resources\TaskResource;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class FrontController extends Controller
{
    public function getTask(Request $request)
    {
        $tasks = Task::query()
            ->orderBy('created_at', 'desc');

        $newTaks = app(Pipeline::class)
            ->send($tasks)
            ->through([
                \App\QueryFilters\Name::class,
                \App\QueryFilters\User::class,
            ])
            ->thenReturn()->get();

        return TaskResource::collection($newTaks);
    }
}
