<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $with = ['user'];

    public const TASK_STATUS_ARRAY = [
        "1" => 'К выполнению',
        "2" => 'В процессе',
        "3" => 'Тестирование',
        "4" => 'Готово',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
