<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

#Configuration 
Inside project directory open terminal and follow instruction
composer install 

then copy .env.example to .env and write there your own db configs

php artisan key:generate
php artisan migrate --seed
php artisan serve 

#First of all add header Accept application/json

# For Crud API`S

## About Users API

api/users/list - get all users

## About Status API

api/status/list - get all statuses

## About Tasks API`s

api/tasks - Get all tasks -> Method Get

api/tasks - Store new task -> Method POST, Fields : name, description, deadline, status_id, user_id

api/tasks/{task_id} - Show spesifik task -> Method Get

api/tasks/{task_id} - Update spesifik task -> Method Put, Fields : name, description, deadline, status_id, user_id

api/tasks/{task_id} - Delete spesifik task - Method destroy

# Front Client Api

/tasks/filter/list - Filter name of task and user : id, name, email
Example : /tasks/filter/list?name=KAmi&user[email]=k@mail.ru
